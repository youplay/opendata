[beam]: https://en.wikipedia.org/wiki/Beam_(website)
[jpeg]: https://en.wikipedia.org/wiki/JPEG
[exif]: https://en.wikipedia.org/wiki/Exif
[etz]: https://en.wikipedia.org/wiki/Eastern_Time_Zone


# Stream timelapse

This directory contains a collection of thumbnails from YouPlay's
 [Beam/Mixer][beam] channel that were dumped every 15 minutes between
 *February 12th* and *July 23rd 2017* and originally uploaded to the project's
 Discord guild.

There's a [JSON file](meta.json) included that contains metadata for each frame,
 such as the timestamp, what game was being played (index pointers) and info
 about special conditions like that the game had just crashed before the
 thumbnail was saved ([example](images/2017/02/21/00500.jpg)).

The images are stored in [JPEG][jpeg] format (that's what the platform served)
 and contain the respective stream frame's timestamp in their [Exif][exif] tag
 (in the original server's local [Eastern Time Zone][etz]).
